package pl.sdacademy.exercises;

public class Starter {

    public static void main(String[] args) {
        final Dog dog = new Dog();
        final Dog pimpek = new Dog("Pimpek");
        pimpek.getName();
        pimpek.setName("pimpus");
    }

}
